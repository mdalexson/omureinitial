# Dmitriy Maletskyi's initial test project - Laravel API

Test project up and running:  
- Base url: **https://omure.promresurs.shop/api**

---

## API's endpoints

### Login
In order to use this API you'll need an Sanctum Auth token:  

URI: **/login/**  
Method: **POST**  
Authorization bearer token: **no**  

Params:  
- **email** - required  
- **password** - required

Example response:  
```
{
    "success": true,
    "errors": [],
    "token": "2|35Y4JVapbslt7GgW2m6LLOotYNrgjSR5XB6HEz5i"
}
```  
---  
### Index  
List of all entries in database  

URI: **/index/**  
Method: **GET**  
Authorization bearer token: **required**  

Example response:  
```
{
    "success": true,
    "contacts": [
        {
            "contact_id": 15,
            "first_name": "Tiago",
            "last_name": "Gomes",
            "email": "geodnnd3edddddd8fddsd5s@gmail.com",
            "phone": null,
            "lead_source": "source example",
            "id": "0035e00000G6mq6AAB",
            "created_at": "2021-12-20T23:34:28.000000Z",
            "updated_at": "2021-12-20T23:40:27.000000Z"
        },
		/* --- ENTRIES -- */
        {
            "contact_id": 16,
            "first_name": "Tom",
            "last_name": "Ripley",
            "email": "tripley@uog.com",
            "phone": null,
            "lead_source": "Public Relations",
            "id": "0035e00000BVJ4eAAH",
            "created_at": "2021-12-20T23:34:28.000000Z",
            "updated_at": "2021-12-20T23:40:27.000000Z"
        }
    ]
    "count": 60
}
```
---
### Read  
Read certain entry from database  

URI: **/read/<contact_id>/**  
Method: **GET**  
Authorization bearer token: **required**  

**WARNING** - "contact_id" is an id of model in database, not Salesforce id!  

Example response:  
```
{
    "success": true,
    "contact": {
        "contact_id": 15,
        "first_name": "Tiago",
        "last_name": "Gomes",
        "email": "geodnnd3edddddd8fddsd5s@gmail.com",
        "phone": null,
        "lead_source": "source example",
        "id": "0035e00000G6mq6AAB",
        "created_at": "2021-12-20T23:34:28.000000Z",
        "updated_at": "2021-12-20T23:40:27.000000Z"
    }
}
```
---
### Store
Create contact in database and dispatch a job to create a contact at Salesforce  
When job is done, updates database entry with salesforce ID  
Job triggers an event of example logging  

URI: **/store/**  
Method: **POST**  
Authorization bearer token: **required**  

Params:  
- **email** - required  
- **last_name** - required  
- **first_name**  
- **phone**  
- **lead_source**  

Example response:  
```
{
    "success": true,
    "id": 346
}
```
---
### Update
Update contact in database and dispatch a job to update a contact at Salesforce  
Job triggers an event of example logging  

URI: **/update/<contact_id>/**  
Method: **PATCH**  
Authorization bearer token: **required**  

**WARNING** - "contact_id" is an id of model in database, not Salesforce id!  

Params:  
- **email**
- **last_name**
- **first_name**  
- **phone**  
- **lead_source** 

Example response:  
```
{
    "success": true
}
```
---
### Delete
Delete contact from database and dispatch a job to delete contact from Salesforce  
Job triggers an event of example logging  

URI: **/delete/<contact_id>/**  
Method: **DELETE**  
Authorization bearer token: **required**  

**WARNING** - "contact_id" is an id of model in database, not Salesforce id!  

Example output:  
```
{
    "success": true
}
```
---
### Sync
Pulls results from Salesforce and insert or update local entries  
Triggers an event of example logging  

URI: **/sync/**  
Method: **POST**  
Authorization bearer token: **required**  

Example output:  
```
{
    "success": true
}
```
---