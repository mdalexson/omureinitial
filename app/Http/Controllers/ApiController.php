<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use App\Repositories\Interfaces\ContactsRepositoryInterface;
use App\Services\Interfaces\ContactsServiceInterface;
use App\Services\Interfaces\SfServiceInterface;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use App\Events\SfSynced;
use App\Jobs\SfStore;
use App\Jobs\SfUpdate;
use App\Jobs\SfDelete;
use \Arr;

class ApiController extends Controller
{
    /**
    * Controller vars
    */
    private $repository, $contactsService, $sfService, $fields;

    /**
    * Repositories, services and fields assigning
    * @return void
    */
    public function __construct(ContactsRepositoryInterface $repository, ContactsServiceInterface $contactsService, SfServiceInterface $sfService)
    {
        $this->repository = $repository;
        $this->contactsService = $contactsService;
        $this->sfService = $sfService;
        $this->fields = [
            'first_name',
            'last_name',
            'email',
            'phone',
            'lead_source'
        ];
    }

    /**
    * List of all entries in database
    * @param Request $request
    * @return string JSON
    */
    public function index(Request $request)
    {
        $contacts = $this->repository->all();
        return response()->json([
            'success' => true,
            'contacts' => $contacts->collect(),
            'count' => $contacts->count()
        ]);
    }

    /**
    * Read certain entry
    * @param Request $request
    * @param int $id
    * @return string JSON
    */
    public function read(Request $request, int $id)
    {
        $contact = $this->repository->getById($id);
        return response()->json([
            'success' => true,
            'contact' => $contact
        ]);
    }

    /**
    * Store entry in database
    * @param Request $request
    * @return string JSON
    */
    public function store(Request $request)
    {

        $credentials = Validator::make($request->all(), [
            'first_name' => ['string:nullable'],
            'last_name' => ['required', 'string'],
            'phone' => ['string:nullable'],
            'email' => ['required', 'email:rfc', 'unique:contacts'],
            'lead_score' => ['string:nullable']
        ]);

        if ($credentials->fails()) {
            throw new ValidationException($credentials);
        }

        $data = $credentials->validated();
        $data = Arr::only($data, $this->fields);

        $contact = $this->contactsService->create($data);

        SfStore::dispatch($contact);

        return response()->json([
            'success' => true,
            'id' => $contact->contact_id
        ]);
    }

    /**
    * Update certain entry in database and dispatch a job to update contact at SalesForce
    * @param Request $request
    * @param int $id
    * @return string JSON
    */
    public function update(Request $request, int $id)
    {
        $credentials = Validator::make($request->all(), [
            'first_name' => ['string:nullable'],
            'last_name' => ['string'],
            'phone' => ['string:nullable'],
            'email' => ['email:rfc', 'unique:contacts'],
            'lead_score' => ['string:nullable']
        ]);

        if ($credentials->fails()) {
            throw new ValidationException($credentials);
        }

        $data = $credentials->validated();
        $data = Arr::only($data, $this->fields);
        $contact = $this->contactsService->update($id, $data);

        SfUpdate::dispatch($contact, $data);

        return response()->json([
            "success" => true
        ]);
    }

    /**
    * Delete certain entry from database and dispatch a job to delete contact at SalesForce
    * @param Request $request
    * @param int $id
    * @return string JSON
    */
    public function delete(Request $request, int $id)
    {

        $sf_id = $this->contactsService->delete($id);

        if(is_string($sf_id)){
            SfDelete::dispatch($sf_id);
        }

        return response()->json([
            "success" => true
        ]);
    }

    /**
    * Sync contacts from Salesforce to databse
    * Create new and update existing
    * @param Request $request
    * @return string JSON
    */
    public function sync(Request $request)
    {
        $contacts = $this->sfService->index();
        $preparedData = [];

        $fields = [
            'id',
            'first_name',
            'last_name',
            'email',
            'lead_source'
        ];

        foreach($contacts['records'] as $c){
            if((bool) $c['is_deleted'] || !strlen($c['email'])){
                continue;
            }

            $data = Arr::only($c, $fields);
            $preparedData[] = $data;
        }

        $result = Contact::upsert($preparedData, 'id', $fields);

        SfSynced::dispatch();

        return response()->json([
            "success" => true
        ]);
    }
}
