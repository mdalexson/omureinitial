<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /**
    * Login via email and password
    * @param Request $request
    * @return json
    * @throws ValidationException
    */
    public function login(Request $request)
    {
        $credentials = Validator::make($request->all(), [
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if ($credentials->fails()) {
            throw new ValidationException($credentials);
        }

        if (Auth::attempt($credentials->validated())) {
            Auth::user()->tokens()->delete();
            $token = Auth::user()->createToken('default');

            return response()->json([
                'success' => true,
                'errors' => [],
                'token' => $token->plainTextToken
            ]);
        }

        throw new ValidationException($credentials);
    }
}
