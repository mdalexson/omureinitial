<?php

namespace App\Jobs;

use App\Services\SfService;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SfDelete implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $sf_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(String $sf_id)
    {
        $this->sf_id = $sf_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SfService $service)
    {
        $service->delete($this->sf_id);
    }
}
