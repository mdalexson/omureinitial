<?php

namespace App\Jobs;

use App\Models\Contact;
use App\Services\SfService;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SfUpdate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $contact, $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Contact $contact, Array $data)
    {
        $this->contact = $contact;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SfService $service)
    {
        $service->update($this->contact, $this->data);
    }
}
