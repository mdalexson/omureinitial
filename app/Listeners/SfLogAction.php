<?php

namespace App\Listeners;

use App\Events\SfStored;
use App\Events\SfUpdated;
use App\Events\SfDeleted;
use App\Events\SfSynced;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SfLogAction
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function handleLog($event)
    {
        /* Temporary log logic, normally use Slack or etc. */
        file_put_contents("/var/www/omure/log.txt", "EVENT TRIGGERED" . PHP_EOL, FILE_APPEND);
        /* --- ETC --- */
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\SfSynced  $event
     * @return void
     */
    public function subscribe($events)
    {
        return [
            SfStored::class => 'handleLog',
            SfUpdated::class => 'handleLog',
            SfDeleted::class => 'handleLog',
            SfSynced::class => 'handleLog'
        ];
    }
}
