<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Interfaces\ContactsRepositoryInterface;
use App\Repositories\ContactsRepository;
use App\Services\Interfaces\ContactsServiceInterface;
use App\Services\ContactsService;
use App\Services\Interfaces\SfServiceInterface;
use App\Services\SfService;

class AppServiceProvider extends ServiceProvider
{

    public $bindings = [
        ContactsRepositoryInterface::class => ContactsRepository::class,
        ContactsServiceInterface::class => ContactsService::class,
        SfServiceInterface::class => SfService::class
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
