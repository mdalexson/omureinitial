<?php

namespace App\Repositories;

use App\Models\Contact;
use App\Repositories\Interfaces\ContactsRepositoryInterface;

class ContactsRepository implements ContactsRepositoryInterface
{
    public function all()
    {
        return Contact::all();
    }

    public function getById(int $id)
    {
        $contact = Contact::findOrFail($id);
        return $contact;
    }
}
