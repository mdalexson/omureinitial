<?php

namespace App\Repositories\Interfaces;

use App\Models\Contact;

interface ContactsRepositoryInterface
{
    public function all();

    public function getById(int $id);
}
