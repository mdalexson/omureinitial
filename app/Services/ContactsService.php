<?php

namespace App\Services;

use App\Models\Contact;
use App\Services\Interfaces\ContactsServiceInterface;
use \Arr;

class ContactsService implements ContactsServiceInterface
{
    public function __construct()
    {
    }

    public function create(Array $data)
    {
        return Contact::create($data);
    }

    public function update(int $id, Array $data)
    {
        $contact = Contact::findOrFail($id);
        $contact->update($data);
        return $contact;
    }

    public function delete(int $id){
        $contact = Contact::findOrFail($id);
        $sf_id = $contact->id;
        $contact->delete();
        return $sf_id;
    }

}
