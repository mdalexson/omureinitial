<?php

namespace App\Services\Interfaces;

use App\Models\Contact;

interface ContactsServiceInterface
{
    public function create(Array $data);
    public function update(int $id, Array $data);
    public function delete(int $id);
}
