<?php

namespace App\Services\Interfaces;

use app\Models\Contact;

interface SfServiceInterface
{
    public function login();
    public function index();
    public function read(String $id);
    public function create(Contact $contact);
    public function update(Contact $contact);
    public function delete(String $id);
}
