<?php

namespace App\Services;

use App\Models\Contact;
use App\Services\Interfaces\SfServiceInterface;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Http;
use App\Events\SfStored;
use App\Events\SfUpdated;
use App\Events\SfDeleted;
use \Arr;

class SfService implements SfServiceInterface
{
    private $base, $email, $password, $token, $fields;

    public function __construct()
    {
        $this->fields = [
            'first_name',
            'last_name',
            'email',
            'lead_source',
            'description',
            'title'
        ];
        $this->base = env("SF_BASE") ?? false;
        if(!$this->base){
            throw new AuthenticationException();
        }

        $this->email = env("SF_EMAIL") ?? false;
        $this->password = env("SF_PASSWORD") ?? false;

        $this->login();
    }

    public function login()
    {
        if(!$this->email || !$this->password){
            throw new AuthenticationException();
        }

        $response = $this->request('post', '/login/', ['email' => $this->email, 'password' => $this->password], false);
        $token = $response['token'];
        $this->token = $token;
    }

    public function index()
    {
        return $this->request('get', '/contacts/');
    }

    public function read(String $id)
    {
        return $this->request('get', "/contacts/$id");
    }

    public function create(Contact $contact)
    {
        $data = $contact->toArray();
        $data = Arr::only($data, $this->fields);

        $result = $this->request('post', '/contacts/', $data);
        if((bool) $result['success']){
            $contact->id = $result['id'];
            $contact->save();

            SfStored::dispatch();
        }
    }

    public function update(Contact $contact)
    {
        $data = $contact->toArray();
        $data = Arr::only($data, $this->fields);

        $result = $this->request('patch', '/contacts/' . $contact->id . '/', $data);
        if((bool) $result['success']){
            SfUpdated::dispatch();
        }
    }

    public function delete(String $id)
    {
        $result = $this->request('delete', "/contacts/$id/");
        if((bool) $result['success']){
            SfDeleted::dispatch();
        }
    }

    private function request(String $method, String $path, Array $data = [], bool $auth = true)
    {
        $headers = [];
        if($auth){
            $headers = ['authorization' => $this->token];
        }

        $result = Http::asForm()->withHeaders($headers)->{$method}($this->base . $path, $data);
        $msg = $result['message'] ?? 'fail';
        if($result->status() !== 200){
            throw new AuthenticationException(); 
        }

        return $result->throw()->collect();
    }

}
