<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\LoginController;
use App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [LoginController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/index', [ApiController::class, 'index']);
    Route::get('/read/{id}', [ApiController::class, 'read']);
    Route::post('/store', [ApiController::class, 'store']);
    Route::patch('/update/{id}', [ApiController::class, 'update']);
    Route::delete('/delete/{id}', [ApiController::class, 'delete']);
    Route::post('/sync', [ApiController::class, 'sync']);
});

Route::fallback(function () {
    return response()->json([
        'success' => false,
        'erorrs' => [
            'global' => 'Wrong path'
        ]
    ], 404);
});
