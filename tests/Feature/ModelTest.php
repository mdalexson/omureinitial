<?php

namespace Tests\Feature;

use App\Models\Contact;
use Database\Factories\ContactFactory;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Database\QueryException;

class ModelTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testModelCreated()
    {
        $model = Contact::factory()->create();
        $email = $model->email;
        $this->assertModelExists($model);
        return $email;
    }

    /**
     * @depends testModelCreated
     * @expectedException QueryException 
     */
    public function testModelShouldNotBeCreated(String $email)
    {
        $this->expectException(QueryException::class);

        $model = Contact::factory()->state([
            'email' => $email
        ])->create();
    }
}
